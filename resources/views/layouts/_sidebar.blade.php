<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      {{-- <img src="../images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8"> --}}
      <span class="brand-text font-weight-light">GI MARKETING</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="../images/default.jpg" class="img-circle elevation-2" alt="User Image" data-toggle="modal" data-target="#edit-profile"/>
      </div>
      <div class="info">
        <a href="#" class="d-block">{{ Auth::user()->f_name }} {{ Auth::user()->l_name }}</a>
      </div>
    </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{ url('dashboard') }}" class="nav-link @if(Request::is('dashboard') || Request::is ('dashboard/*')) active @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          @if (Auth::user()->role_id == null)
          <li class="nav-item">
            <a href="{{ url('employees') }}" class="nav-link @if(Request::is('employees') || Request::is ('employees/*')) active @endif">
                <i class="nav-icon fas fa-user"></i>
                    <p>
                        Employees
                    </p>
            </a>
          </li>
          @endif
          @if (Auth::user()->role_id == null)
          <li class="nav-item has-treeview">
                <a href="#" class="nav-link @if(Request::is('department') || Request::is('department/*')) active @endif">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p>
                        Departments
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ URL('department/positions') }}" class="nav-link @if(Request::is('department/positions') || Request::is('department/positions/*')) active @endif">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Positions</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ URL('department/roles') }}" class="nav-link @if(Request::is('department/roles') || Request::is('department/roles/*')) active @endif">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Roles</p>
                    </a>
                </li>
            </ul>
          </li>
          @endif
          @if (Auth::user()->role_id == 1 || Auth::user()->role_id == null)
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link @if(Request::is('columns') || Request::is('columns/*')) active @endif">
                <i class="nav-icon fas fa-columns"></i>
                <p>
                    Columns
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
            <a href="{{ URL('columns/video-types') }}" class="nav-link @if(Request::is('columns/video-types') || Request::is('columns/video-types/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Video Type</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ URL('columns/strategy') }}" class="nav-link @if(Request::is('columns/strategy') || Request::is('columns/strategy/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Strategy</p>
                </a>
            </li>
            <li class="nav-item">
              <a href="{{ URL('columns/campaigns') }}" class="nav-link @if(Request::is('columns/campaigns') || Request::is('columns/campaigns/*')) active @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Campaign</p>
              </a>
          </li>
            <li class="nav-item">
            <a href="{{ URL('columns/stores') }}" class="nav-link @if(Request::is('columns/stores') || Request::is('columns/stores/*')) active @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Store</p>
              </a>
            </li>
            <li class="nav-item">
            <a href="{{ URL('columns/sla') }}" class="nav-link @if(Request::is('columns/sla') || Request::is('columns/sla/*')) active @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>SLA</p>
              </a>
            </li>
        </ul>
      </li>
        @endif
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link @if(Request::is('imports') || Request::is('imports/*')) active @endif">
                <i class="nav-icon fas fa-file-import"></i>
                <p>
                    Imported
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ URL('imports') }}" class="nav-link @if(Request::is('imports') || Request::is('imports/*')) active @endif">
                    <i class="far fa-circle nav-icon"></i>
                    <p>All Products</p>
                </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pending to Create Videos</p>
              </a>
          </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Work in Progress</p>
                </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pending Approval</p>
              </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Running Ads</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Paused Ads</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Killed Ads</p>
            </a>
          </li>
        </ul>
      </li>
          <li class="nav-item">
            <a href="{{ url('settings') }}" class="nav-link @if(Request::is('settings') || Request::is('settings/*')) active @endif">
              <i class=" nav-icon fas fa-cog"></i>
              <p>Settings</p>
            </a>
          </li>
          <li class="nav-item">
          <a href="#" class="nav-link"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="nav-icon fas fa-sign-out-alt"></i>
            <p>
              Logout
            </p>
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>