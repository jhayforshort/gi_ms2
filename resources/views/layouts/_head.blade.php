<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Marketing System</title>
  <!-- Favicon -->
  <link rel="icon" type="image/png" href="../images/icon.png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../css/backend/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="../css/backend/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../css/backend/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="../css/backend/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../css/backend/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../css/backend/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../css/backend/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="../css/backend/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Main -->
  <link rel="stylesheet" href="../css/backend/main.css">
  <link rel="stylesheet" href="../css/backend/ekko-lightbox.css">

  <link rel="stylesheet" href="../css/backend/select2.min.css">
  <link rel="stylesheet" href="../css/backend/select2-bootstrap4.min.css">