<!-- jQuery -->
<script src="../js/backend/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../js/backend/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="../js/backend/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="../js/backend/Chart.min.js"></script>
<!-- Sparkline -->
<script src="../js/backend/sparkline.js"></script>
<!-- JQVMap -->
<script src="../js/backend/jquery.vmap.min.js"></script>
<script src="../js/backend/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="../js/backend/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../js/backend/moment.min.js"></script>
<script src="../js/backend/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="../js/backend/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="../js/backend/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="../js/backend/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="../js/backend/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../js/backend/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../js/backend/demo.js"></script>

<script src="../js/backend/select2.full.min.js"></script>

<script src="../js/backend/jquery.inputmask.min.js"></script>
<script src="../js/backend/moment.min.js"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()


  })
</script>