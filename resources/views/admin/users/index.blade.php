@extends('main')

@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            @if (Session::has('success'))

            <div class="alert alert-success" role="alert">
                <strong>Success:</strong> {{ Session::get('success') }}
            </div>

            @endif

            @if (count($errors) > 0)
            <div class="alert alert-danger" role="alert">
                <strong>Errors:</strong>
                <ul>
                @foreach ($errors->all() as $error)
                    <li> {{ $error }} </li>
                @endforeach
                </ul>
            </div>
            @endif
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>All Employees</h1>
                </div>
                <div class="modal fade" id="create-employee">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Create New User</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="{{ route('employees.store') }}"> @csrf
                                <div class="form-group">
                                    <label>First Name </label>
                                    <input class="form-control" type="text" id="f_name" name="f_name" placeholder="First Name" required autocomplete="f_name" autofocus>
                                </div>
                                <div class="form-group">
                                    <label>Last Name </label>
                                    <input class="form-control" type="text" id="l_name" name="l_name" placeholder="Last Name" required autocomplete>
                                </div>
                                <div class="form-group">
                                    <label>Email </label>
                                    <input class="form-control" type="text" id="email" name="email" placeholder="Email" required autocomplete>
                                </div>
                                <div class="form-group">
                                    <label>Password </label>
                                    <input class="form-control" type="password" id="password" name="password" placeholder="Password" required autocomplete>
                                </div>
                                <div class="form-group">
                                    <label>Positions </label>
                                    <select class="select2" multiple="multiple" name="position_id[]" id="position_id" data-placeholder="Select a position" style="width: 100%;" required autcomplete="position_id">
                                        @foreach ($positions as $position)
                                        <option value="{{ $position->id }}"> {{$position->name}} </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Roles</label>
                                    <select class="form-control" name="role_id" id="role_id" required autocomplete="role_id">
                                      <option value="" selected> --Select Role-- </option>
                                      @foreach ($roles as $role)
                                      <option value="{{ $role->id }}"> {{$role->name}} </option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <div class="col-xm-6">
                            <ol class="breadcrumb float-sm-right">
                                <button class="btn btn-primary btn-md" data-toggle="modal" data-target="#create-employee">Add <i class="fas fa-plus"></i></button>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Position</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr>
                            <td>{{ $user->f_name }} {{ $user->l_name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                            @if (!empty($user->roles->name))
                            {{ $user->roles->name }}
                            @else
                            
                            @endif
                            </td>
                            <td>
                            @foreach ($user->positions as $position)
                            @if (!empty($position->name))
                            <li>{{$position->name}}</li>
                            @else

                            @endif
                            @endforeach
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#edit-employee-{{$user->id}}">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <div class="modal fade" id="edit-employee-{{$user->id}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit User</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                <form method="POST" action="{{ route('employees.update', $user->id) }}">@csrf
                                                @method('PUT')
                                                    <div class="form-group">
                                                        <label>First name</label>
                                                        <input class="form-control" type="text" id="f_name" name="f_name" value="{{ $user->f_name }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last Name</label>
                                                        <input class="form-control" type="text" id="l_name" name="l_name" value="{{ $user->l_name }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input class="form-control" type="email" id="email" name="email" value="{{ $user->email }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Positions </label>
                                                        <select class="select2" multiple="multiple" name="position_id[]" style="width: 100%;">
                                                            @foreach ($positions as $position)
                                                            <option value="{{ $position->id }}" {{ $user->positions->contains($position->id) ? 'selected' : ''}}> {{$position->name}} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Roles</label>
                                                        <select class="form-control" name="role_id" id="role_id" required autocomplete="role_id">
                                                          @foreach ($roles as $role)
                                                          <option value="{{ $role->id }}" @if($user->role_id == $role->id) selected @endif> {{$role->name}} </option>
                                                          @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="modal-footer justify-content-between">
                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                </div>
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-default">
                                        <i class="fas fa-eye"></i>
                                    </button>
                                    <button type="button" class="btn btn-default">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </div>
                            </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection