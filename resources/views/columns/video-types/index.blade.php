@extends('main')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            @if (Session::has('success'))

            <div class="alert alert-success" role="alert">
                <strong>Success:</strong> {{ Session::get('success') }}
            </div>

            @endif

            @if (count($errors) > 0)
            <div class="alert alert-danger" role="alert">
                <strong>Errors:</strong>
                <ul>
                @foreach ($errors->all() as $error)
                    <li> {{ $error }} </li>
                @endforeach
                </ul>
            </div>
            @endif
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>All Video Types</h1>
                </div>
                <div class="modal fade" id="create-video-type">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Video Types</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                            <form method="POST" action="{{ route('video-types.store') }}">
                            @csrf
                                <div class="form-group">
                                    <input class="form-control" type="text" id="name" name="name" placeholder="Create New Video Types" required autocomplete="name" style="text-transform: capitalize;" autofocus>
                                </div>
                                <textarea class="form-control" id="description" name="description" placeholder="Description..."></textarea>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <div class="col-xm-6">
                            <ol class="breadcrumb float-sm-right">
                                <button class="btn btn-primary btn-md" data-toggle="modal" data-target="#create-video-type">Add <i class="fas fa-plus"></i></button>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Descriptions</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($videotypes as $videotype)
                            <tr>
                            <td style="text-transform: capitalize;">{{$videotype->name}}</td>
                            <td>{{$videotype->description}}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#edit-video-type-{{$videotype->id}}">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <div class="modal fade" id="edit-video-type-{{$videotype->id}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Video Type</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                <form method="POST" action="{{ route('video-types.update', $videotype->id) }}">@csrf
                                                @method('PUT')
                                                    <div class="form-group">
                                                        <input class="form-control" type="text" id="name" name="name" value="{{$videotype->name}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea class="form-control" id="description" name="description">{{$videotype->description}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer justify-content-between">
                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                </div>
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-default">
                                        <i class="fas fa-eye"></i>
                                    </button>
                                    <button type="button" class="btn btn-default">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </div>
                            </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection