@extends('main')

@push('stylesheets')

<link rel="stylesheet" href="../cs/backend/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../cs/backend/responsive.bootstrap4.min.css">

@endpush

@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                <strong>Success:</strong> {{ Session::get('success') }}
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger" role="alert">
                <strong>Errors:</strong>
                <ul>
                @foreach ($errors->all() as $error)
                    <li> {{ $error }} </li>
                @endforeach
                </ul>
            </div>
            @endif
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>All Products</h1>
                </div>
                <div class="modal fade" id="create-products">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Create New Import</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                            <form method="POST" action="{{ route('imports.store') }}">
                            @csrf
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <input class="form-control" style="text-transform: capitalize;" type="text" id="product_name" name="product_name" placeholder="Create New Product" required autocomplete="product_name" style="text-transform: capitalize;" autofocus>
                                </div>
                                <div class="form-group">
                                    <label>Store</label>
                                    <select class="form-control" name="store_id" id="store_id" style="text-transform: capitalize;" data-placeholder="Select Store" style="width: 100%;" required autcomplete="store_id">
                                        <option value=""> --Select Store-- </option>
                                        @foreach($stores as $store)
                                        <option value="{{$store->id}}">{{$store->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Campaign</label>
                                    <select class="form-control" name="campaign_id" id="campaign_id" data-placeholder="Select Campaign" style="width: 100%;" required autocomplete="campaign_id">
                                        <option value=""> --Select Campaign-- </option>
                                        @foreach($campaigns as $campaign)
                                        <option value="{{$campaign->id}}" style="text-transform: capitalize;">{{$campaign->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                <label>Video Type</label>
                                    <select class="form-control" name="video_type_id" id="video_type_id" data-placeholder="Select Video Type" style="width: 100%; text-transform: capitalize;" required autcomplete="video_type_id">
                                        <option value=""> --Select Video Type-- </option>
                                        @foreach ($video_types as $video_type)
                                        <option value="{{$video_type->id}}" style="text-transform: capitalize;">{{$video_type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                <label>Strategy</label>
                                    <select class="form-control" name="strategy_id" id="strategy_id" data-placeholder="Select Strategy" style="width: 100%;" required autcomplete="strategy_id">
                                        <option value=""> --Select Strategy-- </option>
                                        @foreach($strategies as $strategy)
                                        <option value="{{$strategy->id}}" style="text-transform: capitalize;">{{$strategy->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                <label>Priority</label>
                                    <select class="form-control" name="priority_id" id="priority_id" data-placeholder="Select Priority" style="width: 100%;" required autcomplete="priority_id">
                                        <option value=""> --Select Priority-- </option>
                                        @foreach($priorities as $priority)
                                        <option value="{{$priority->id}}" style="text-transform: capitalize;">{{$priority->name}}</option>
                                        @endforeach
                                    </select>
                                </div>  
                                {{-- <div class="form-group">
                                    <label>Date Assign to Create</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask>
                                    </div>
                                </div> --}}
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <div class="col-xm-6">
                            <ol class="breadcrumb float-sm-right">
                                <button class="btn btn-primary btn-md" data-toggle="modal" data-target="#create-products">Add <i class="fas fa-plus"></i></button>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Product Name</th>
                      <th>Store</th>
                      <th>Campaign</th>
                      <th>Video Type</th>
                      <th>Strategy</th>
                      <th>SLA</th>
                      <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($imports as $import)
                    <tr>
                      <td style="text-transform: capitalize;">
                            {{ Str::limit($import->product_name, 45 ) }}
                      </td>
                      <td style="text-transform: uppercase;">{{$import->stores->code}}</td>
                      <td style="text-transform: uppercase;">{{$import->campaigns->code}}</td>
                      <td style="text-transform: capitalize;">{{$import->videotypes->name}}</td>
                      <td style="text-transform: capitalize;">{{$import->strategies->name}}</td>
                      <td style="text-transform: capitalize;">{{$import->priorities->name}}</td>
                      <td>
                        <a class="btn btn-primary btn-sm" href="{{ Route('imports.show', $import->id)}}">
                            <i class="fas fa-folder">
                            </i>
                            View
                        </a>
                        <a class="btn btn-success btn-sm" type="button" style="color: white;" data-toggle="modal" data-target="#edit-product-{{$import->id}}">
                            <i class="fas fa-edit" style="color: white;">
                            </i>
                            Edit
                        </a>
                        <div class="modal fade" id="edit-product-{{$import->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Edit</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="POST" action="{{ route('imports.update', $import->id)}}">@csrf
                                        @method('PUT')
                                        <div class="form-group">
                                            <label>Product Name</label>
                                            <input class="form-control" style="text-transform: capitalize;" type="text" id="product_name" name="product_name" value="{{$import->product_name}}" required autocomplete="product_name">
                                        </div>
                                        <div class="form-group">
                                            <label>Store</label>
                                            <select class="form-control" name="store_id" id="store_id" style="text-transform: capitalize;" style="width: 100%;" required autocomplete="store_id">
                                                @foreach($stores as $store)
                                                <option value="{{$store->id}}" @if($import->store_id == $store->id) selected @endif>{{$store->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Campaign</label>
                                            <select class="form-control" name="campaign_id" id="campaign_id" style="text-transform: capitalize;" style="width: 100%;" required autocomplate="campaign_id">
                                                @foreach($campaigns as $campaign)
                                                <option value="{{$campaign->id}}" @if($import->campaign_id == $campaign->id) selected @endif>{{$campaign->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Video Type</label>
                                            <select class="form-control" name="video_type_id" id="video_type_id" style="text-transform: capitalize;" style="width: 100%;" required autocomplate="video_type_id">
                                                @foreach($video_types as $video_type)
                                                <option value="{{$video_type->id}}" @if($import->video_type_id == $video_type->id) selected @endif>{{$video_type->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Strategy</label>
                                            <select class="form-control" name="strategy_id" id="strategy_id" style="text-transform: capitalize;" style="width: 100%;" required autocomplate="strategy_id">
                                                @foreach($strategies as $strategy)
                                                <option value="{{$strategy->id}}" @if($import->strategy_id == $strategy->id) selected @endif>{{$strategy->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>SLA</label>
                                            <select class="form-control" name="priority_id" id="priority_id" style="text-transform: capitalize;" style="width: 100%;" required autocomplate="priority_id">
                                                @foreach($priorities as $priority)
                                                <option value="{{$priority->id}}" @if($import->priority_id == $priority->id) selected @endif>{{$priority->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-danger btn-sm" href="#">
                            <i class="fas fa-trash">
                            </i>
                            Delete
                        </a>
                    </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
        </div>
    </section>
</div>

@push('scripts')

<script>

$(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
});

</script>

<script src="../js/backend/jquery.dataTables.min.js"></script>
<script src="../js/backend/dataTables.bootstrap4.min.js"></script>
<script src="../js/backend/dataTables.responsive.min.js"></script>
<script src="../js/backend/responsive.bootstrap4.min.js"></script>

@endpush

@endsection