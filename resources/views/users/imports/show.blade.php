@extends('main')

@push('stylesheets')

<link rel="stylesheet" href="../css/backend/ekko-lightbox.css">

@endpush

@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                <strong>Success:</strong> {{ Session::get('success') }}
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger" role="alert">
                <strong>Errors:</strong>
                <ul>
                @foreach ($errors->all() as $error)
                    <li> {{ $error }} </li>
                @endforeach
                </ul>
            </div>
            @endif
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 style="text-transform: capitalize;">{{$imports->product_name}} <span class="badge badge-success">Created at: {{ date('F j, Y - g:i A', strtotime($imports->created_at)) }}</span></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <button type="button" class="btn btn-primary btn-md">Update</button>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2">
                    <!-- Thumbnail -->
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="form-group text-center">
                                <label>Thumbnail</label>
                            </div>
                            <div class="col-sm-12 text-center">
                                <a href="../images/default.jpg" data-toggle="modal" data-title="{{$imports->product_name}}" data-gallery="gallery">
                                  <img src="../images/default.jpg" class="img-fluid mb-2" alt="white sample"/>
                                </a>
                            </div>
                            <div class="col-sm-12 text-center">
                                <a href="#" class="btn btn-primary btn-block">Upload</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-header p-2">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Store</label>
                                    <input type="text" class="form-control" style="text-transform: capitalize;" placeholder="{{$imports->stores->name}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Campaign</label>
                                    <input type="text" class="form-control" style="text-transform: capitalize;" placeholder="{{$imports->campaigns->name}}" disabled>
                                </div>
                                <div class="form-group">
                                        <label>Video Type</label>
                                    <input type="text" class="form-control" style="text-transform: capitalize;" placeholder="{{$imports->videotypes->name}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Strategy</label>
                                    <input type="text" class="form-control" style="text-transform: capitalize;" placeholder="{{$imports->strategies->name}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label>SLA</label>
                                    <input type="text" class="form-control" style="text-transform: capitalize;" placeholder="{{$imports->priorities->name}}" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Product Research</h3>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Supplier Name</label>
                                            <input type="text" class="form-control" placeholder="Enter Supplier Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Supplier</label>
                                            <input type="text" class="form-control" placeholder="Enter Supplier">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Product Link</label>
                                            <input type="text" class="form-control" placeholder="Enter Product Link">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Base Price</label>
                                            <input type="text" class="form-control" placeholder="Enter Base Price">
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Creatives</h3>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Supplier Name</label>
                                            <input type="text" class="form-control" placeholder="Enter Supplier Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Supplier</label>
                                            <input type="text" class="form-control" placeholder="Enter Supplier">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Product Link</label>
                                            <input type="text" class="form-control" placeholder="Enter Product Link">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Base Price</label>
                                            <input type="text" class="form-control" placeholder="Enter Base Price">
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Ads / Web Dev / Bot</h3>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Supplier Name</label>
                                            <input type="text" class="form-control" placeholder="Enter Supplier Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Supplier</label>
                                            <input type="text" class="form-control" placeholder="Enter Supplier">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Product Link</label>
                                            <input type="text" class="form-control" placeholder="Enter Product Link">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Base Price</label>
                                            <input type="text" class="form-control" placeholder="Enter Base Price">
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Videos</h3>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="card-body box-profile">
                                    <div class="col-sm-12 text-center">
                                        <a href="../images/default.jpg" data-toggle="modal" data-title="{{$imports->product_name}}" data-gallery="gallery">
                                            <img src="../images/default.jpg" class="img-fluid mb-2" alt="white sample"/>
                                        </a>
                                    </div>
                                    <div class="col-sm-12 text-center">
                                        <a href="#" class="btn btn-primary btn-block">Upload</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Notes</h3>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@push('scripts')

<script src="../js/backend/ekko-lightbox.min.js"></script>

<script>
    $(function () {
      $(document).on('click', '[data-toggle="modal"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
          alwaysShowClose: true
        });
      });
  
      $('.filter-container').filterizr({gutterPixels: 3});
      $('.btn[data-filter]').on('click', function() {
        $('.btn[data-filter]').removeClass('active');
        $(this).addClass('active');
      });
    })
  </script>

@endpush