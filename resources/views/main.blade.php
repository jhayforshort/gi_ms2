<!DOCTYPE html>
<html>

<head>
@include('layouts._head')

@yield('stylesheets')
</head>

<body class="hold-transition sidebar-mini layout-fixed">

@include('layouts._sidebar')

@yield('content')

@include('layouts._footer')

@include('layouts._javascripts')

@stack('scripts')

</body>

</html>