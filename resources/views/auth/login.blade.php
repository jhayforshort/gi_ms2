<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, 
    initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <!-- Login Module-->
    <div class="login">
        <div class="wrap">
            <!-- Toggle -->
            <div id="toggle-wrap">
                <div id="toggle-terms">
                    <div id="cross">
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
            <!-- Terms -->
            <!-- <div class="terms">
                <h2>Terms of Service</h2>
                <p class="small">Last modified: August 14, 2020</p>
                <h3>Welcome to Marketing System</h3>
                <p>By using our Services, you are agreeing to these terms. Please read them carefully.</p>
                <p>Our services are very diverse, so sometimes additional terms or product requirements (including age requirements) may apply</p>
            </div> -->
            <!-- Recovery -->
            <div class="recovery">
                <h2>Password Recovery</h2>
                <p>Enter either the <strong>email address</strong> or <strong>username</strong> on the account and <strong>click Submit</strong></p>
                <p>We'll email instructions on how to reset your password.</p>
                <form class="recovery-form" action="" method="post">
                    <input type="text" class="input" id="user_recover" placeholde="Enter Email or Username Here">
                    <input type="submit" class="button" value="Submit">
                </form>
                <p class="mssg">An email has been sent to you with further instructions.</p>
            </div>
            <!-- Slider -->
            <div class="content">
                <div class="logo">
                    <a href="#"><img src="icon.png" alt=""></a>
                </div>
                <!-- Slideshow -->
                <div id="slideshow">
                    <div class="one">
                        <h2><span>PR</span></h2>
                        <p>Sign up to attend any of hundreds of events nationwide</p>
                    </div>
                    <div class="two">
                        <h2><span>Creatives</span></h2>
                        <p>Thousands of instant online classes/tutorials included</p>
                    </div>
                    <div class="three">
                        <h2><span>Web Dev</span></h2>
                        <p>Create your own groups and connect with others that share your interests</p>
                    </div>
                    <div class="four">
                        <h2><span>Ads/Bot</span></h2>
                        <p>Share your works and knowledge with the community on the public showcase section</p>
                    </div>
                </div>
            </div>
            <!-- Login Form -->
            <div class="user">
                <div class="form-wrap">
                    <!-- Tabs -->
                    <div class="tabs">
                        <h3 class="login-tab"><a class="log-in active" href="#login-tab-content"><span>Login</span></a></h3>
                    </div>
                    <!-- Tabs Content -->
                    <div class="tabs-content">
                        <!--Tabs Content Login -->
                        <div id="login-tab-content" class="active">
                            <form class="login-form" action="{{ route('login') }}" method="post">
                                @csrf    
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong style="color: white;">{{$message}}</strong>
                                </span>
                                @enderror
                                <input type="text" class="input @error('email') is-invalid @enderror" name="email" id="email" autocomplete="email" placeholder="Email">
                            
                                <input type="password" class="input @error('password') is-invalid @enderror" name="password" id="password" autocomplete="password" placeholder="Password">
                             
                                <input type="checkbox" class="checkbox" checked id="remember_me">
                                <label for="remember_me">Remember Me</label>
                                <input type="submit" class="bot" value="Login">
                            </form>
                            <div class="help-action">
                                <p><i class="fa fa-arrow-left" aria-hidden="true"></i><a class="forgot" href="#">Forgot your password</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/demo.js"></script>
</body>
</html>