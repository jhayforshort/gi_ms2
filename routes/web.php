<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('dashboard', 'Admin\DashboardController');
Route::resource('employees', 'Admin\UsersController');
Route::resource('department/positions', 'Admin\Departments\PositionsController');
Route::resource('department/roles', 'Admin\Departments\RolesController');
Route::resource('settings', 'SettingsController');

Route::resource('imports', 'Imported\AllProductsController');
Route::match(['get','post'], 'imports'. 'Imported\AllProductsController@updateProductName')->name('imports.updateProductName');

Route::resource('columns/video-types', 'Columns\VideoTypeController');
Route::resource('columns/strategy', 'Columns\StrategyController');
Route::resource('columns/stores', 'Columns\StoresController');
Route::resource('columns/sla', 'Columns\PriorityController');
Route::resource('columns/campaigns', 'Columns\CampaignController');