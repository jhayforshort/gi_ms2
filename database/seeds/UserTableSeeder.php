<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::create([
            'f_name' => "Green",
            'l_name' => "Ink",
            'email' => "greenink@gmail.com",
            'password' => Hash::make('GreenInk2019!'),
        ]);

        $users->email_verified_at = Carbon::now();
        $users->save();
    }
}
