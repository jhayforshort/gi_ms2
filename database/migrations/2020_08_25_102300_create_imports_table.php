<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_name');
            $table->bigInteger('video_type_id')->unsigned()->nullable();
            $table->foreign('video_type_id')->references('id')->on('video_types');
            $table->bigInteger('priority_id')->unsigned()->nullable();
            $table->foreign('priority_id')->references('id')->on('priorities');
            $table->bigInteger('store_id')->unsigned()->nullable();
            $table->foreign('store_id')->references('id')->on('stores');
            $table->bigInteger('strategy_id')->unsigned()->nullable();
            $table->foreign('strategy_id')->references('id')->on('strategies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imports');
    }
}
