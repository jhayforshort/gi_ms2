<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
    protected $fillable = [
        'product_name' , 'video_type_id', 'priority_id', 'store_id', 'strategy_id',
    ];

    public function videotypes()
    {
        return $this->belongsTo('App\VideoType', 'video_type_id');
    }

    public function priorities()
    {
        return $this->belongsTo('App\Priority', 'priority_id');
    }

    public function stores()
    {
        return $this->belongsTo('App\Store', 'store_id');
    }

    public function strategies()
    {
        return $this->belongsTo('App\Strategy', 'strategy_id');
    }

    public function campaigns()
    {
        return $this->belongsTo('App\Campaign', 'campaign_id');
    }
}
