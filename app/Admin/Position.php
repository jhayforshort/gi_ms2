<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{

    protected $fillable = [
        'name',
        'user_id',
        'position_id',
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_positions');
    }
}
