<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Admin\Role;
use App\Admin\Position;

use Session;
use Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::user()->role_id == null)
        {
        $roles = Role::all();
        $positions = Position::all();
        $users = User::all();
        return view('admin.users.index', compact('roles','users','positions'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'f_name' => 'required|max:20',
            'l_name' => 'required|max:20',
            'email' => 'required|unique:users',
            'password' => 'required|max:12',
            'role_id' => 'required',
            'position_id' => 'required',
        ));

        $data = $request->all();
        $users = new User;
        $users->f_name = $data['f_name'];
        $users->l_name = $data['l_name'];
        $users->email = $data['email'];
        $users->role_id = $data['role_id'];
        $users->password = Hash::make($data['password']);
        $users->save();

        $positions = Position::find($data['position_id']);
        $users->positions()->attach($positions);

        Session::flash('success', 'User Create was Successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $users = User::find($id);

        $this->validate($request, array(
            'f_name' => 'required|max:20',
            'l_name' => 'required|max:20',
            'email' => 'required',
            'role_id' => 'required',
            'position_id' => 'required',
        ));

        $data = $request->all();
        $users =  User::find($id);
        $users->f_name = $data['f_name'];
        $users->l_name = $data['l_name'];
        $users->email = $data['email'];
        $users->role_id = $data['role_id'];
        $users->save();

        $users->positions()->detach();
        $positions = Position::find($data['position_id']);
        $users->positions()->attach($positions);
        Session::flash('success', 'User Update was Successfully');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
