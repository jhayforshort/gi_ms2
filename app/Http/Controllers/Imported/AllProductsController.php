<?php

namespace App\Http\Controllers\Imported;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Priority;
use App\VideoType;
use App\Store;
use App\Strategy;
use App\Import;
use App\Campaign;

use App\User;

use Carbon\Carbon;
use Session;
use Auth;

class AllProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $imports = Import::all();
        $video_types = VideoType::all();
        $priorities = Priority::all();
        $strategies = Strategy::all();
        $stores = Store::all();
        $campaigns = Campaign::all();
        return view('users.imports.index', compact('stores','imports','video_types','strategies','priorities','campaigns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'product_name' => 'required|max:200',
            'strategy_id' => 'required',
            'video_type_id' => 'required',
            'priority_id' => 'required',
            'store_id' => 'required',
            'campaign_id' => 'required',
        ));

        $data = $request->all();
        $imports = new Import;
        $imports->product_name = $data['product_name'];
        $imports->video_type_id = $data['video_type_id'];
        $imports->strategy_id = $data['strategy_id'];
        $imports->priority_id = $data['priority_id'];
        $imports->store_id = $data['store_id'];
        $imports->campaign_id = $data['campaign_id'];
        $imports->created_at = Carbon::now();
        $imports->save();
        
        Session::flash('success', 'Product Created was successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $stores = Store::all();
        $imports = Import::find($id);
        $campaigns = Campaign::all();
        $video_types = VideoType::all();
        $strategies = Strategy::all();
        $priorities = Priority::all();
        return view('users.imports.show',compact('imports','stores','campaigns','video_types','strategies','priorities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $data = $request->all();
        $imports = Import::find($id);
        $imports->product_name = $data['product_name'];
        $imports->store_id = $data['store_id'];
        $imports->campaign_id = $data['campaign_id'];
        $imports->video_type_id = $data['video_type_id'];
        $imports->strategy_id = $data['strategy_id'];
        $imports->priority_id = $data['priority_id'];
        $imports->save();
        
        Session::flash('success', 'Updated was successfully!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
