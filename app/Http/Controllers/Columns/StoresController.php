<?php

namespace App\Http\Controllers\Columns;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use Session;

use App\Store;

class StoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == null)
        {
        $stores = Store::all();
        return view('columns.store.index',compact('stores'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required|max:20',
            'code' => 'required|max:3',
        ));

        $data = $request->all();
        $stores = new Store;
        $stores->name = $data['name'];
        $stores->code = $data['code'];
        $stores->save();

        Session::flash('success', 'Stores Created was successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'name' => 'required|max:20',
            'code' => 'required|max:3',
        ));

        $data = $request->all();
        $stores = Store::find($id);
        $stores->name = $data['name'];
        $stores->code = $data['code'];
        $stores->save();

        Session::flash('success', 'Stores Updated was successfully!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
