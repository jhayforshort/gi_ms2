<?php

namespace App\Http\Controllers\Columns;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Strategy;

use Auth;

use Session;

class StrategyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id == 1 || Auth::user()->role_id == null)
        {
        $strategies = Strategy::all();
        return view('columns.strategy.index',compact('strategies'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('columns.strategy.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required',
            'description' => 'required',
        ));

        $data = $request->all();
        $strategies = new Strategy;
        $strategies->name = $data['name'];
        $strategies->description = $data['description'];
        $strategies->save();

        Session::flash('success', 'Strategy was created successfully!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'name' => 'required|max:20',
            'description' => 'required',
        ));

        $data = $request->all();
        $strategies = Strategy::find($id);
        $strategies->name = $data['name'];
        $strategies->description = $data['description'];
        $strategies->save();

        Session::flash('success', 'Strategy Update was successfully!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
